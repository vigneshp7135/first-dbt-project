SELECT 
    D.department_name , 
    L.city, 
    L.state_province
FROM
    {{source('public','departments')}} D 
JOIN {{source('public','locations')}}  L
ON  D.location_id = L.location_id
