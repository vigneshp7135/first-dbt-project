SELECT d.department_name,
       e.*
FROM {{source('public','departments')}}  d
JOIN
(SELECT
    count(employee_id) AS no_of_employees,
    department_id
FROM
    {{source('public','employees')}} 
GROUP BY department_id) e USING (department_id)