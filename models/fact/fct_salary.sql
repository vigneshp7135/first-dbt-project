SELECT
    E.first_name, 
    E.last_name, 
    E.salary, 
    J.job_title
FROM {{source('public','employees')}}  E 
JOIN {{source('public','jobs')}}   J
ON E.salary BETWEEN J.min_salary AND J.max_salary