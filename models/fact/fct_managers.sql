{{config(
    "materialized" = 'table',
    "tags" = 'fct_employee'
)}}
SELECT
    E.first_name AS "Employee Name",
    M.first_name AS "Manager"
FROM {{source('public','employees')}} E 
LEFT OUTER JOIN {{source('public','employees')}} M
ON E.manager_id = M.employee_id