SELECT
    first_name || ' ' || last_name AS Employee_name,
    employee_id,
    country_name 
FROM
    {{source('public','employees')}}  
JOIN
    {{source('public','departments')}}  
USING(department_id) 
JOIN
    {{source('public','locations')}}  
USING( location_id) 
JOIN
    {{source('public','countries')}}  
USING ( country_id);