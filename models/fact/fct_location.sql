{{config(
    materialized='table'
) }}
SELECT 
    E.first_name,
    E.last_name, 
    D.department_name, 
    L.city, 
    L.state_province
FROM {{source('public','employees')}} E 
JOIN {{source('public','departments')}} D  
ON E.department_id = D.department_id  
JOIN locations L
ON D.location_id = L.location_id