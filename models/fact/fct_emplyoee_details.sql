{{config(
    materialized='table'
) }}
SELECT
    E.first_name , 
    E.last_name , 
    E.department_id , 
    D.department_name 
FROM {{source('public','employees')}} E 
JOIN {{source('public','departments')}}  D 
ON E.department_id = D.department_id